class Users {
  constructor(
    id,
    name,
    email,
    password,
    dob,
    city,
    state,
    status,
    created_at,
    updated_at
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.dob = dob;
    this.city = city;
    this.state = state;
    this.status = status;
    this.created_at = created_at;
    this.updated_at = updated_at;
  }
}

module.exports = {
  Users: Users,
};
