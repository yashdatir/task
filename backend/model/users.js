const { getConnection } = require('typeorm');
const Users = require('../classModel/users').Users;

const register_user = async ({
  name,
  email,
  password,
  dob,
  city,
  state,
  status,
}) => {
  try {
    return await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Users)
      .values({
        name,
        email,
        password,
        dob: Date(dob),
        city,
        state,
        status,
        created_at: new Date(),
        updated_at: new Date(),
      })
      .execute();
  } catch (err) {
    throw err;
  }
};

const update_user = async ({
  id,
  name,
  email,
  password,
  dob,
  city,
  state,
  status,
}) => {
  try {
    return await getConnection()
      .createQueryBuilder()
      .update(Users)
      .set({
        name,
        email,
        password,
        dob: Date(dob),
        city,
        state,
        status,
        updated_at: new Date(),
      })
      .where('id = :id', { id })
      .execute();
  } catch (err) {
    throw err;
  }
};
module.exports = {
  register: register_user,
  update: update_user,
};
