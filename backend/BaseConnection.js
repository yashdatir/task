const { createConnection } = require('typeorm');
require('dotenv').config();

BaseConnection = async () => {
  const connection = await createConnection({
    type: 'mysql',
    host: 'localhost',
    port: '3306',
    username: 'root',
    password: 'C-24maikrupa',
    database: 'task',
    synchronize: true,
    logging: false,
    entities: [require('./entity/userSchema')],
  });
  return connection;
};

module.exports = BaseConnection();
