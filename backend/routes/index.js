var express = require('express');
var router = express.Router();

const user = require('../model/users');

router.post('/register', async function (req, res) {
  return res.send({
    message: 'successful',
    status: '200',
    results: await user.register(req.body),
  });
});

router.put('/update', async function (req, res) {
  return res.send({
    message: 'successful',
    status: '200',
    results: await user.update(req.body),
  });
});

module.exports = router;
