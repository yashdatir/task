const EntitySchema = require('typeorm').EntitySchema;
const Users = require('../classModel/users').Users;

module.exports = new EntitySchema({
  name: 'Users',
  target: Users,
  columns: {
    id: {
      primary: true,
      type: 'bigint',
      generated: true,
    },
    name: {
      type: 'varchar',
      nullable: true,
    },
    email: {
      type: 'varchar',
      nullable: true,
    },
    password: {
      type: 'varchar',
      nullable: true,
    },
    dob: {
      type: 'timestamp',
      nullable: true,
    },
    city: {
      type: 'varchar',
      nullable: true,
    },
    state: {
      type: 'varchar',
      nullable: true,
    },
    status: {
      type: 'tinyint',
      nullable: false,
      default: 1,
    },
    created_at: {
      type: 'timestamp',
      nullable: true,
    },
    updated_at: {
      type: 'timestamp',
      nullable: true,
    },
  },
});
